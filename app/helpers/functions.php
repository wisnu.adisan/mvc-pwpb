<?php
function url($path){
    return (BASE_URL . "/". trim($path, "/"));

}

function redirect($string) {
    header("Location: " . BASE_URL . "/$string");
    exit;
}

function notnull($data){
   return strlen($data) > 0 ? true : false;
}

function asset($path){
    return BASE_URL . "/assets" . "/" . trim($path, "/");
}

function lowercase($data){
    if(preg_match("/[a-z]/", $data)) { 
        return true;
    } else {
        return false;
    }
}

function minLength($data) {
    if(preg_match("/[a-z]{5,}/", $data)){
        return true;
    } else {
        return false;
    }
}

function validateInput($data){
    return htmlspecialchars($data);
}


?>