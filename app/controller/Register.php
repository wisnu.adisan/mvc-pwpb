<?php

require_once("../app/core/Controller.php");

class Register extends Controller {
    public function __construct(){
        if(!empty($_SESSION['username']) && $_GET['url'] != 'login/logout'){
            redirect('home/');
        }
    }
    public function index(){
        $data['title']= 'Login';
        $data['data'] = 'Ini Sudah Halaman Login';
        $this->view('register/index', $data);
    }
    public function store(){
        if(isset($_POST['password']) && $_POST['password'] == $_POST['konfirm_password']){
            if(strlen($_POST['password']) >= 8){
                if(lowercase($_POST['username'])){
                    if(minLength($_POST['username'])){
                        if($this->model('Auth')->register($_POST) > 0){
                            Flasher::setFlash('User added, please login', '', 'success');
                            redirect("login/");
                        } 
                    } else {
                        Flasher::setFlash('Username must be at least 5 characters','','danger');
                        redirect("register/");
                    }
                } else {
                    Flasher::setFlash('Username must be lowercase and only string','','danger');
                    redirect("register/");
                }
                        
            } else {
                Flasher::setFlash('Password must be at least 8 characters','','danger');
                redirect("register/");
            }
        } else {
            Flasher::setFlash('Password must be match!', '', 'danger');
            $this->view('register/index');
        } 
    }

    
}


?>
