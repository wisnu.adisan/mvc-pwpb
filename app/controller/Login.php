<?php

require_once("../app/core/Controller.php");

class Login extends Controller {
    public function __construct(){
        if(!empty($_SESSION['username']) && $_GET['url'] != 'login/logout'){
            redirect('home/');
        }
    }
    public function index(){
        $data['title']= 'Login';
        $data['data'] = 'Ini Sudah Halaman Login';
        $this->view('login/index', $data);
    }

    public function store(){
        $loggedInUser = $this->model('Auth')->login($_POST);
        if($loggedInUser){
            Flasher::setFlash('Login Success', '', 'success');
            ($this->model('Auth')->createSession($loggedInUser));
        } else {
            $data['title'] = 'Login';
            Flasher::setFlash('Emai/Username tidak tersedia', '', 'danger');
            $this->view('login/index', $data);
        }
    }

    public function logout(){
        $_SESSION = array();

        Flasher::setFlash('Logout success', '', 'success');
        redirect("register/");
    }

    
}


?>
